= CityCAR - Back

With automatic compilation :

[source]
----
npm run dev
----

----------------- or -------------------------

Without automatic compilation :

[source]
----
npm run start
----
