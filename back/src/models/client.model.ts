export {}

const MONGOOSE = require('mongoose');
const SCHEMA = MONGOOSE.Schema;

const CLIENT_SCHEMA = new SCHEMA({
    name: {
        type: String,
        unique: true,
        required: true
    },
    cars: [{
        type: SCHEMA.Types.ObjectId,
        ref: 'Car'
    }] 
});

module.exports = MONGOOSE.model('Client', CLIENT_SCHEMA);
