export {}

const MONGOOSE = require('mongoose');
const SCHEMA = MONGOOSE.Schema;

const CAR_SCHEMA = new SCHEMA({
    registration: {
        type: String,
        unique: true
    },
    mark: {
        type: String
    },
    model: {
        type: String
    }
});

module.exports = MONGOOSE.model('Car', CAR_SCHEMA);
