export {}

const MONGOOSE = require('mongoose');
const SCHEMA = MONGOOSE.Schema;

const USER_SCHEMA = new SCHEMA({
    email: {
        type: String,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,;3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        unique: true,
        required: true 
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum : ['gerant', 'technicien'],
        required: true
    }
});

module.exports = MONGOOSE.model('User', USER_SCHEMA);
