export {}

const MONGOOSE = require('mongoose');
const SCHEMA = MONGOOSE.Schema;

const REPAIR_SCHEMA = new SCHEMA({
    client: {
        type: SCHEMA.Types.ObjectId,
        ref: 'Client',
        required: true
    },
    car: {
        type: SCHEMA.Types.ObjectId,
        ref: 'Car'
    },
    repair_type: {
        type: String,
        enum: ['Carrosserie', 'Mécanique', 'Service-Minute'],
        required: true
    },
    end_date: {
        type: Date,
        default: Date.now,
        required: true
    }
    
});

module.exports = MONGOOSE.model('Repair', REPAIR_SCHEMA);
