import { Request, Response, NextFunction } from "express";

const JWT_UTILS = require('../utils/jwt.util');

module.exports = {
    allRoutes: (req: Request, res: Response, next: NextFunction) => {
        if (JWT_UTILS.decodeToken(req.headers.authorization?.replace("Bearer ", "")).role === 'gerant'
        || JWT_UTILS.decodeToken(req.headers.authorization?.replace("Bearer ", "")).role === 'technicien')
        return next();
        else
        return res.status(403).json({message: "Non autorisation pour accéder à cette ressource."});
    },
    
    repairsGetRoute: (req: Request, res: Response, next: NextFunction) => {
        if (JWT_UTILS.decodeToken(req.headers.authorization?.replace("Bearer ", "")).role === 'gerant')
        return next();
        else
        return res.status(403).json({message: "Non autorisation pour accéder à cette ressource."});
    }
}
