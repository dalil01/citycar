import { Request, Response } from "express";

const CLIENT = require('../models/client.model');
const CAR = require('../models/car.model');
const REPAIR = require('../models/repair.model');

module.exports = {
    add: async (req: Request, res: Response) => {
        const CLIENT_ID = await CLIENT.findOne({name: req.body.clientName}, '_id');
        const CAR_ID = await CAR.findOne({registration: req.body.registration.toUpperCase()}, '_id') || '';
        
        new REPAIR({
            client: CLIENT_ID._id,
            car: CAR_ID._id,
            repair_type: req.body.repairType
        }).save((saveErr: Error) => {
            if (!saveErr) return res.status(200).json({message: "Réparation enregistrée dans la BD."});
            else return res.status(400).json({message: saveErr});
        });
    },
    
    get: async (req: Request, res: Response) => {
        return res.status(200).json({repairs: await REPAIR.find({}).populate('client car')});
    }
}
