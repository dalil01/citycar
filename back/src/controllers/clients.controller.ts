import { Request, Response } from "express";

const CLIENT = require('../models/client.model');
const CAR = require('../models/car.model');

module.exports = {
    add: async (req: Request, res: Response) => {
        if (await CLIENT.exists({name: req.body.clientName})) {
            return res.status(200).json({message: "Client déjà enregistré dans la BD."});
        } else {
            new CLIENT({
                name: req.body.clientName
            }).save((saveErr: Error) => {
                if (!saveErr) return res.status(200).json({message: "Client enregistré dans la BD."});
                else return res.status(400).json({message: saveErr});
            });
        }
    },
    
    addCar: async (req: Request, res: Response) => {
        const CLIENT_OBJ = await CLIENT.findOne({name: req.body.clientName});
        const CAR_ID = await CAR.findOne({registration: req.body.registration.toUpperCase()}, '_id');
        
        if (!CLIENT_OBJ.cars.includes(CAR_ID._id)) {
            CLIENT_OBJ.cars.push(CAR_ID._id);
            CLIENT_OBJ.save();
            return res.status(200).json({message: "Voiture désormais affiliée au client."});
        } else {
            return res.status(200).json({message: "Voiture déjà affiliée au client."});
        }
    }
}
