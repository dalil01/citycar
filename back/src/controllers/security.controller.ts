import { Request, Response } from "express";

const USER = require('../models/user.model');
const BCRYPT = require ('bcrypt');
const JWT_UTILS = require('../utils/jwt.util');

module.exports = {
    register: async (req: Request, res: Response) => {
        const EMAIL: string = req.body.email;
        const PASSWORD: string = req.body.password;
        const IS_USER_EXISTANT: boolean = await USER.findOne({email: EMAIL}) !== null;
        
        if (await USER.exists({email: req.body.email})) {
            return res.status(400).json({message: "Utilisateur déjà enregistré dans la BD.", name: "AlreadyRegisteredUser"});
        } else {
            BCRYPT.hash(PASSWORD, 5, (bcryptErr: Error, bcryptPassword: String) => {
                if (bcryptErr) return res.status(400).json({message: bcryptErr});
                else {
                    let USER_ROLE: string = 'technicien';
                    let OWNER_CODE_INVALID: boolean = false;
                    if (req.body.ownerCode) if (req.body.ownerCode === 'CityCAR') { // Code à modifier, selon la préférence du gérant.
                        USER_ROLE = "gerant"
                    } else {
                        OWNER_CODE_INVALID = true;
                        return res.status(400).json({message: "Code invalide.", name: "InvalidOwnerCode"});
                    }
                    
                    if (!OWNER_CODE_INVALID) {
                        const NEW_USER = new USER({
                            email: EMAIL,
                            password: bcryptPassword,
                            role: USER_ROLE
                        });
                        
                        NEW_USER.save((saveErr: Error, saveRes: Response) => {
                            if (saveErr) return res.status(400).json({message: saveErr});
                            else return res.status(200).json({message: saveRes});
                        });
                    }
                }
            });
        }
    },
    
    login: async (req: Request, res: Response) => {
        const EMAIL: string = req.body.email;
        const PASSWORD: string = req.body.password;
        const SEARCHED_USER = await USER.findOne({email: EMAIL});
        
        if (SEARCHED_USER !== null) {
            BCRYPT.compare(PASSWORD, SEARCHED_USER.password, (err: Error, BcryptRes: Response) => {
                if (BcryptRes) return res.status(200).json({token: JWT_UTILS.generateToken(SEARCHED_USER)});
                return res.status(404).json({message: "Mot de passe incorrect.", name: "IncorrectPassword"});
            });
        } else return res.status(404).json({message: "Utilisateur non identifié.", name: "UnindentifiedUser"});
    }
}
