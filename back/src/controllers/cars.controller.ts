import { Request, Response } from "express";

const AXIOS = require('axios');
const CAR = require('../models/car.model');

module.exports = {
    add: async (req: Request, res: Response) => {
        const CLIENT_REGISTRATION: string = req.body.registration;
        if (await CAR.exists({registration: CLIENT_REGISTRATION})) {
            return res.status(200).json({message: "Voiture déjà enregistrée dans la BD."});
        } else {
            // URL API: https://apiplaqueimmatriculation.com/tester-lapi/
            AXIOS.get("http://api.apiplaqueimmatriculation.com/carte-grise?immatriculation=" + req.body.registration + "&token=TokenDemo&format=json")
            .then((response: any, error: Error) => {
                if (error) return res.status(400).json({message: error});
                else {
                    new CAR({
                        registration: CLIENT_REGISTRATION,
                        mark: response.data.data.marque,
                        model: response.data.data.modele
                    }).save((saveErr: Error) => {
                        if (!saveErr) return res.status(200).json({message: "Voiture enregistrée dans la BD."});
                        else return res.status(400).json({message: saveErr});
                    });
                }
            });
        }
    }
}
