import Server from "./utils/server.util";
import Database from "./utils/database.util";

const DATABASE: Database = new Database("localhost", 27017);
const SERVER: Server = new Server(4000);

DATABASE.connect("CityCar");

SERVER.start();

// middlewares
SERVER.useModule(require("body-parser").urlencoded({extended: true}));
SERVER.useModule(require("body-parser").json());
SERVER.useModule(require("cors")());

// routes
SERVER.useModule(require("./app-routing"));
