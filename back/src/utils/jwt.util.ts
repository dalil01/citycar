const JWT = require('jsonwebtoken');
const JWT_SIGNATURE_SECRET = "3qaidn1n37nitre0q04xb0gi8yuu3qmb3oisebzlh44owjolxh";

module.exports = {
    generateToken: (userData: any) => {
        return JWT.sign({
            email: userData.email,
            role: userData.role 
        },
        JWT_SIGNATURE_SECRET
        );
    },
    
    decodeToken: (token: string) => {
        return JWT.verify(token, JWT_SIGNATURE_SECRET);
    }
}
