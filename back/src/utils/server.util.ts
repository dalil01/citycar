const EXPRESS: any = require("express")();

export default class Server {
    private readonly port: number;

    constructor(port: number) {
        this.port = port;
    }

    start(): void {
        EXPRESS.listen(this.port, () => console.log("Server started on the port : " + this.port));
    }
    
    useModule(module: string): void {
        EXPRESS.use(module);
    }
}
