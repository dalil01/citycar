const MONGOOSE = require("mongoose");

export default class Database {
    private readonly host: string;
    private readonly port: number;
    
    constructor(host: string, port: number) {
        this.host = host;
        this.port = port;
    }
    
    connect(dbName: string): void {
        MONGOOSE.connect("mongodb://" + this.host + ':' + this.port + '/' + dbName,
        {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
            autoIndex: true
        },
        (error: Error) => {
            (!error)
            ? console.log("MongoDB connected on port : " + this.port)
            : console.log("Connection failed : " + error);
        });
    }
}
