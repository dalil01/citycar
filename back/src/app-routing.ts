const EXPRESS = require('express');
const ROUTER = new EXPRESS.Router;

const AUTH_MIDDLEWARE = require('./firewall/auth');
const SECURITY_CONTROLLER = require('./controllers/security.controller');
const CLIENT_CONTROLLER = require('./controllers/clients.controller');
const CAR_CONTROLLER = require('./controllers/cars.controller');
const REPAIR_CONTROLLER = require('./controllers/repairs.controller');

// Security
ROUTER.post('/register', SECURITY_CONTROLLER.register);
ROUTER.post('/login', SECURITY_CONTROLLER.login);

// Car
ROUTER.post('/clients/add', AUTH_MIDDLEWARE.allRoutes, CLIENT_CONTROLLER.add);
ROUTER.post('/clients/cars/add', AUTH_MIDDLEWARE.allRoutes, CLIENT_CONTROLLER.addCar);

// Car
ROUTER.post('/cars/add', AUTH_MIDDLEWARE.allRoutes, CAR_CONTROLLER.add);

// Repair
ROUTER.post('/repairs/add', AUTH_MIDDLEWARE.allRoutes, REPAIR_CONTROLLER.add);
ROUTER.get('/repairs/get', AUTH_MIDDLEWARE.repairsGetRoute, REPAIR_CONTROLLER.get);

module.exports = ROUTER;
