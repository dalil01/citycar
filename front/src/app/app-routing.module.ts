import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalledClientsComponent } from "./components/called-clients/called-clients.component";
import { RepairsManagementComponent } from "./components/repairs-management/repairs-management.component";
import { RepairsListComponent } from "./components/repairs-list/repairs-list.component";

const routes: Routes = [
  { path: '', component: RepairsManagementComponent},
  { path: 'calledClients', component: CalledClientsComponent },
  { path: 'repairsList', component: RepairsListComponent },
  { path: '**', component: RepairsManagementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
