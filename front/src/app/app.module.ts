import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { CalledClientsComponent } from './components/called-clients/called-clients.component';
import { RepairsListComponent } from './components/repairs-list/repairs-list.component';
import { RepairsManagementComponent } from './components/repairs-management/repairs-management.component';

import { SecurityService } from './services/security/security.service';
import { RepairService } from './services/repair/repair.service';
import { RegisterComponent } from './components/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    CalledClientsComponent,
    RepairsListComponent,
    RepairsManagementComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    SecurityService,
    RepairService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
