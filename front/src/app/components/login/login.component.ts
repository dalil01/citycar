import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SecurityService } from 'src/app/services/security/security.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @Input() loginModalIsOpen!: boolean;
  @Output() loginModalStateEmitter = new EventEmitter<boolean>();
  @Output() isAuthEmitter = new EventEmitter<boolean>();
  public registerModalIsOpen = false;
  
  public loginForm!: FormGroup;
  public emailError!: boolean; public passwordError!: boolean;
  public emailErrorMessage!: string; public passwordErrorMessage!: string;
  
  constructor(private securityService: SecurityService) { }
  
  
  ngOnInit(): void { this.initLoginForm(); }
  
  initLoginForm(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });
  }
  
  toggleModal(): void {
    this.loginModalIsOpen = !this.loginModalIsOpen;
    this.loginModalStateEmitter.emit(this.loginModalIsOpen);
  }
  
  public onSubmitLoginForm(): void {
    this.emailError = false; this.passwordError = false;
    this.emailErrorMessage = ""; this.passwordErrorMessage = "";
    
    if (!this.loginForm.value.email) {
      this.emailError = true;
      this.emailErrorMessage = "Champs vide.";
    }
    if (!this.loginForm.value.password) {
      this.passwordError = true;
      this.passwordErrorMessage = "Champs vide.";
    }
    if (this.loginForm.value.email && this.loginForm.value.password) {
      this.securityService
      .sendLoginRequest(
        this.loginForm.value.email,
        this.loginForm.value.password
        
        ).subscribe(loginRes => {
          this.securityService.setTokenInLocalStorage(loginRes.token);
          this.loginForm.reset();
          this.toggleModal();
          
        }, loginErr => {
          if (loginErr.error.name === 'IncorrectPassword') {
            this.passwordError = true;
            this.passwordErrorMessage = "Mot de passe incorrect."
          } else {
            this.emailError = true; this.passwordError = true;
          }
        });
        
      }
    }
    
    public updateRegisterModalState(event: boolean): void {
      this.toggleModal();
      this.registerModalIsOpen = event;
    }
  }
