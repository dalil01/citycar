import { Component, OnDestroy, OnInit } from '@angular/core';
import { RepairService } from 'src/app/services/repair/repair.service';
import { FormControl, FormGroup } from "@angular/forms";
import { SecurityService } from 'src/app/services/security/security.service';
import { Subscription } from 'rxjs';
import { RepairsInProgress } from 'src/app/types/repairsInprogress.type';
import { RepairTypeType } from 'src/app/types/repairTypeType.type';
import { RepairRESTService } from 'src/app/services/repairREST/repair-rest.service';

@Component({
  selector: 'app-repairs-management',
  templateUrl: './repairs-management.component.html',
  styleUrls: ['./repairs-management.component.css']
})
export class RepairsManagementComponent implements OnInit, OnDestroy {
  public isAuth: boolean = false;
  private isAuthSubscription!: Subscription;
  
  public callForm!: FormGroup;
  public clientNameError!: boolean; public registrationError!: boolean; public repairTypeError!: boolean;
  public clientNameErrorMessage!: string; public registrationErrorMessage!: string;
  
  public repairsInProgress!: RepairsInProgress;
  
  constructor(
    private repairService: RepairService,
    private securityService: SecurityService,
    private repairRESTService: RepairRESTService
    ) { }
    
    ngOnInit(): void {
      this.isAuth = this.securityService.isAuth();
      this.isAuthSubscription = this.securityService
      .getIsTokenFromLocalStorageSubject()
      .subscribe(() => {
        this.isAuth = this.securityService.isAuth();    
      });
      this.repairsInProgress = this.repairService.getRepairsInProgress();  
      this.initCallForm();
    }
    
    ngOnDestroy(): void { this.isAuthSubscription?.unsubscribe(); }
    
    private initCallForm(): void {
      this.callForm = new FormGroup({
        repairType: new FormControl(),
        clientName: new FormControl(),
        registration: new FormControl()
      });
    }
    
    public onSubmitCallForm(): void {
      this.clientNameError = false; this.registrationError = false; this.repairTypeError = false;
      if (!this.callForm.value.clientName) {
        this.clientNameError = true;
        this.clientNameErrorMessage = "Champs vide.";
      }
      if (!this.callForm.value.repairType) {
        this.repairTypeError = true;
      } else if (this.repairService.hasRepairStarted(this.callForm.value.repairType)) {
        this.repairTypeError = true;
        alert("Le poste pour cette réparation est déjà occupé.");
      }
      if (this.callForm.value.registration) {
        if (!this.callForm.value.registration.match(/^[A-Z]{2}[-][0-9]{3}[-][A-Z]{2}$/i)) {
          this.registrationError = true;
          this.registrationErrorMessage = "Format d'immatriculation invalide.";
        } else {
          this.repairRESTService.getCarInfos(this.callForm.value.registration).subscribe((res: any) => {
            if (res.data.erreur) {
              this.registrationError = true;
              this.registrationErrorMessage = res.data.erreur;
            }
            
            if (!this.clientNameError && !this.registrationError && !this.repairTypeError) {
              this.repairService.callClient(
                this.callForm.value.repairType,
                this.callForm.value.clientName.charAt(0).toUpperCase() + this.callForm.value.clientName.slice(1),
                this.callForm.value.registration.toUpperCase());
                this.callForm.reset();
              }
            });
          }
        } else {
          if (!this.clientNameError && !this.registrationError && !this.repairTypeError) {
            this.repairService.callClient(
              this.callForm.value.repairType,
              this.callForm.value.clientName.charAt(0).toUpperCase() + this.callForm.value.clientName.slice(1),
              '');
              this.callForm.reset();
            }
          }
        }
        
        public startRepair(repairType: RepairTypeType): void { this.repairService.startRepair(repairType); }
        
        public endRepair(repairType: RepairTypeType, clientName: string, registration: string): void {
          if (registration) {
            this.repairRESTService.addClient(clientName).subscribe(() => {
              this.repairRESTService.addCar(registration).subscribe(() => {
                this.repairRESTService.linkCarToClient(clientName, registration).subscribe(() => {
                  this.repairRESTService.addRepair(clientName, registration, repairType).subscribe(() => {
                    alert("Réparation enregistrée.");
                    this.repairService.endRepair(repairType);
                  });
                });
              });
            });
          } else {
            this.repairRESTService.addClient(clientName).subscribe(() => {
              this.repairRESTService.addRepair(clientName, registration, repairType).subscribe(() => {
                alert("Réparation enregistrée.");
                this.repairService.endRepair(repairType);
              });
            });
          }
        }
        
        public cancelRepair(repairType: RepairTypeType): void { this.repairService.endRepair(repairType); }
        
        public hasRepairBeenCalled(repairType: RepairTypeType): boolean { return this.repairService.hasRepairBeenCalled(repairType); }
        
        public hasRepairStarted(repairType: RepairTypeType): boolean { return this.repairService.hasRepairStarted(repairType); }
        
        public getRepairStateColor(repairType: RepairTypeType): string {
          if (this.repairService.hasRepairBeenCalled(repairType)) return 'orange';
          else if (this.repairService.hasRepairStarted(repairType)) return 'red';
          else return '#3EE54F';
        }
        
        public haveAllRepairsStarted(): boolean { return this.repairService.haveAllRepairsStarted(); }
      }
