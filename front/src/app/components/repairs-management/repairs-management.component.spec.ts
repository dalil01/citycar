import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairsManagementComponent } from './repairs-management.component';

describe('RepairsManagementComponent', () => {
  let component: RepairsManagementComponent;
  let fixture: ComponentFixture<RepairsManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepairsManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
