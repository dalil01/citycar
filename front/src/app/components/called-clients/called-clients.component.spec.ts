import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalledClientsComponent } from './called-clients.component';

describe('CalledClientsComponent', () => {
  let component: CalledClientsComponent;
  let fixture: ComponentFixture<CalledClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalledClientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalledClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
