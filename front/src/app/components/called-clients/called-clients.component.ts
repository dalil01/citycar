import { Component, OnInit } from '@angular/core';
import { RepairService } from 'src/app/services/repair/repair.service';
import { RepairsInProgress } from 'src/app/types/repairsInprogress.type';
import { RepairStateType } from 'src/app/types/repairState.type';

@Component({
  selector: 'app-called-clients',
  templateUrl: './called-clients.component.html',
  styleUrls: ['./called-clients.component.css']
})
export class CalledClientsComponent implements OnInit {
  private repairsInProgress!: RepairsInProgress;
  
  public carrosserieState!: RepairStateType;
  public mecaniqueState!: RepairStateType;
  public serviceMinuteState!: RepairStateType;
  
  public carrosserieStateColor!: string;
  public mecaniqueStateColor!: string;
  public serviceMinuteStateColor!: string;
  
  public carrosserieRegistration!: string;
  public mecaniqueRegistration!: string;
  public serviceMinuteRegistration!: string;
  
  constructor(private repairService: RepairService) { }
  
  ngOnInit(): void {
    this.repairsInProgress = this.repairService.getRepairsInProgress();
    this.updateRepairsState(); this.updateRepairsRegistration();
    
    window.onstorage = (event: any) => {
      if (event.key === 'REPAIRS_IN_PROGRESS') {
        this.repairsInProgress = JSON.parse(event.newValue);
        this.updateRepairsState(); this.updateRepairsRegistration();
      }
    };
  }
  
  private updateRepairsState(): void {
    this.carrosserieState = this.repairsInProgress[0].repairState;
    this.mecaniqueState = this.repairsInProgress[1].repairState;
    this.serviceMinuteState = this.repairsInProgress[2].repairState;
    
    if (this.carrosserieState === 'called') this.carrosserieStateColor = 'orange';
    else if (this.carrosserieState === 'started') this.carrosserieStateColor = 'red';
    else this.carrosserieStateColor = '#3EE54F';
    
    if (this.mecaniqueState === 'called') this.mecaniqueStateColor = 'orange';
    else if (this.mecaniqueState === 'started') this.mecaniqueStateColor = 'red';
    else this.mecaniqueStateColor = '#3EE54F';
    
    if (this.serviceMinuteState === 'called') this.serviceMinuteStateColor = 'orange';
    else if (this.serviceMinuteState === 'started') this.serviceMinuteStateColor = 'red';
    else this.serviceMinuteStateColor = '#3EE54F';
  }
  
  private updateRepairsRegistration(): void {
    this.carrosserieRegistration = this.repairsInProgress[0].repair.registration;
    this.mecaniqueRegistration = this.repairsInProgress[1].repair.registration;
    this.serviceMinuteRegistration = this.repairsInProgress[2].repair.registration;
  }
}
