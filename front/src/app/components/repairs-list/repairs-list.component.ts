import { Component, OnInit } from '@angular/core';
import { SecurityService } from "src/app/services/security/security.service";
import { Router } from "@angular/router";
import { RepairService } from 'src/app/services/repair/repair.service';
import { RepairRESTService } from 'src/app/services/repairREST/repair-rest.service';

@Component({
  selector: 'app-repairs-list',
  templateUrl: './repairs-list.component.html',
  styleUrls: ['./repairs-list.component.css']
})
export class RepairsListComponent implements OnInit {
  public usedRepairsList: Array<any>;
  private allRepairsList: Array<any>;
  public repairsDateList: Array<string>;
  
  constructor(
    private repairRESTService: RepairRESTService,
    private securityService: SecurityService,
    private router: Router
    ) {
      this.usedRepairsList = [];
      this.allRepairsList = [];
      this.repairsDateList = [];
    }
    
    ngOnInit(): void {
      if (!this.securityService.getTokenInLocalStorage()
      || this.securityService.getRole(this.securityService.getTokenInLocalStorage()) !== 'gerant') {
        this.router.navigateByUrl('/');
      } else {
        this.repairRESTService.getRepairsList().subscribe((data: any) => {
          this.allRepairsList = data.repairs;
          this.usedRepairsList = this.allRepairsList;
          
          this.usedRepairsList.forEach((repair: any) => {
            const REPAIR_END_DATE = repair.end_date.substr(0, 10);
            if (!this.repairsDateList.includes(REPAIR_END_DATE))
            this.repairsDateList.push(REPAIR_END_DATE);
          });
        });
      }
    }
    
    public filterRepairsByDate(date: string = ''): void {
      if (date) {
        this.usedRepairsList = [];
        this.usedRepairsList = this.allRepairsList.filter(repair => repair.end_date.substr(0, 10) === date);
      } else this.usedRepairsList = this.allRepairsList;
    }
  }
