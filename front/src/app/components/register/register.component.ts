import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SecurityService } from 'src/app/services/security/security.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Input() registerModalIsOpen!: boolean;
  @Output() registerModalStateEmitter = new EventEmitter<boolean>();
  @Output() isAuthEmitter = new EventEmitter<boolean>();
  public ownerCodeIsOpen!: boolean;
  
  public registerForm!: FormGroup;
  public emailError!: boolean; public passwordError!: boolean; public ownerCodeError!: boolean;
  public emailErrorMessage!: string; public passwordErrorMessage!: string; public ownerCodeErrorMessage!: string; 
  
  constructor(private securityService: SecurityService) { }
  
  ngOnInit(): void { this.initRegisterForm(); }
  
  toggleModal(): void {
    this.registerModalIsOpen = !this.registerModalIsOpen;
    this.registerModalStateEmitter.emit(this.registerModalIsOpen);
  }
  
  toggleOwnerCodeInput(): void { this.ownerCodeIsOpen = !this.ownerCodeIsOpen; }
  
  initRegisterForm(): void {
    this.registerForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
      ownerCode: new FormControl()
    });
  }
  
  public onSubmitRegisterForm(): void {
    this.emailError = false; this.passwordError = false; this.ownerCodeError = false;
    this.emailErrorMessage = ""; this.passwordErrorMessage = ""; this.ownerCodeErrorMessage = "";
    
    if (!this.registerForm.value.email) {
      this.emailError = true;
      this.emailErrorMessage = "Champs vide.";
    }
    if (!this.registerForm.value.password) {
      this.passwordError = true;
      this.passwordErrorMessage = "Champs vide.";
    }
    if (this.registerForm.value.email && this.registerForm.value.password) {
      this.securityService.sendRegisterRequest(
        this.registerForm.value.email,
        this.registerForm.value.password,
        this.registerForm.value.ownerCode
        ).subscribe(() => {
          alert("Enregistrement réussie.");
          
          this.securityService.sendLoginRequest(
            this.registerForm.value.email,
            this.registerForm.value.password
            
            ).subscribe(loginRes => {
              this.securityService.setTokenInLocalStorage(loginRes.token);
              this.registerForm.reset();
              this.toggleModal();
            });
            
          }, (registerErr) => {
            if (registerErr.error.name === 'AlreadyRegisteredUser') {
              this.emailError = true;
              this.emailErrorMessage = "Utilisateur déjà existant. Veuillez saisir un mail différent.";
            }
            if (registerErr.error.message.name === 'ValidationError') {
              this.emailError = true;
              this.emailErrorMessage = "Mail invalide.";
            }
            if (registerErr.error.name === 'InvalidOwnerCode') {
              this.ownerCodeError = true;
              this.ownerCodeErrorMessage = "Code secret incorrect.";
            }
          });
          
        }
      }
    }
