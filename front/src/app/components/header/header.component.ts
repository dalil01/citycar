import { Component, OnDestroy, OnInit } from '@angular/core';
import { SecurityService } from "src/app/services/security/security.service";
import { Router } from "@angular/router";
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private isAuthSubscription!: Subscription;
  public isAuth: boolean = false;
  public userRole: string = '';
  public registerModalIsOpen = false;
  public loginModalIsOpen = false;
  
  constructor(private securityService: SecurityService, private router: Router) { }
  
  ngOnInit(): void {
    this.isAuth = this.securityService.isAuth();
    if (this.isAuth) this.userRole = this.securityService.getRole(this.securityService.getTokenInLocalStorage());
    
    this.isAuthSubscription = this.securityService
    .getIsTokenFromLocalStorageSubject()
    .subscribe(() => {
      this.isAuth = this.securityService.isAuth();
      if (this.isAuth) this.userRole = this.securityService.getRole(this.securityService.getTokenInLocalStorage());
    });
  }
  
  ngOnDestroy(): void { this.isAuthSubscription?.unsubscribe(); }
  
  public updateRegisterModalState(event: boolean): void { this.registerModalIsOpen = event; }
  
  public updateLoginModalState(event: boolean): void { this.loginModalIsOpen = event; }
  
  public logout(): void {
    this.securityService.removeTokenFromLocalStorage();
    this.router.navigateByUrl('/');
  }
}
