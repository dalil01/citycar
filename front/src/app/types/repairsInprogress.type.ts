import { Repair } from "../classes/repair";
import { RepairStateType } from "./repairState.type";

export type RepairsInProgress = [
    { // Carosserie
        repair: Repair,
        repairState: RepairStateType
    },
    { // Mécanique
        repair: Repair,
        repairState: RepairStateType
    },
    { // Service-Minute
        repair: Repair,
        repairState: RepairStateType
    }
];
