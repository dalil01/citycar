import { Injectable } from '@angular/core';
import { RepairsInProgress } from 'src/app/types/repairsInprogress.type';
import { Repair } from 'src/app/classes/repair';
import { RepairTypeType } from 'src/app/types/repairTypeType.type';

@Injectable({
  providedIn: 'root'
})
export class RepairService {
  private repairsInProgress: RepairsInProgress;
  private REPAIRS_IN_PROGRESS_KEY: string = 'REPAIRS_IN_PROGRESS';
  
  constructor() {
    if (this.getRepairsInProgressInLocalStorage()) {
      this.repairsInProgress = this.getRepairsInProgressInLocalStorage();
    } else {
      this.repairsInProgress = [
        {
          repair: new Repair('Carrosserie'),
          repairState: ''
        },
        {
          repair: new Repair('Mécanique'),
          repairState: ''
        },
        {
          repair: new Repair('Service-Minute'),
          repairState: ''
        }
      ];
    }
  }
  
  public callClient(repairType: RepairTypeType, clientName: string, registration: string): void {
    if (repairType === 'Carrosserie') {
      this.repairsInProgress[0].repair.clientName = clientName;
      this.repairsInProgress[0].repair.registration = registration;
      this.repairsInProgress[0].repairState = 'called';
    } else if (repairType === 'Mécanique') {
      this.repairsInProgress[1].repair.clientName = clientName;
      this.repairsInProgress[1].repair.registration = registration;
      this.repairsInProgress[1].repairState = 'called';
    } else {
      this.repairsInProgress[2].repair.clientName = clientName;
      this.repairsInProgress[2].repair.registration = registration;
      this.repairsInProgress[2].repairState = 'called';
    }
    this.setRepairsInProgressInLocalStorage();
  }
  
  public startRepair(repairType: RepairTypeType): void {
    if (repairType === 'Carrosserie') {
      this.repairsInProgress[0].repairState = 'started';
    } else if (repairType === 'Mécanique') {
      this.repairsInProgress[1].repairState = 'started';
    } else {
      this.repairsInProgress[2].repairState = 'started';
    }
    this.setRepairsInProgressInLocalStorage();
  }
  
  public endRepair(repairType: RepairTypeType): void {
    if (repairType === 'Carrosserie') {
      this.repairsInProgress[0].repair = new Repair('Carrosserie');
      this.repairsInProgress[0].repairState = '';
    } else if (repairType === 'Mécanique') {
      this.repairsInProgress[1].repair = new Repair('Mécanique');
      this.repairsInProgress[1].repairState = '';
    } else {
      this.repairsInProgress[2].repair = new Repair('Service-Minute');
      this.repairsInProgress[2].repairState = '';
    }
    this.setRepairsInProgressInLocalStorage();
  }
  
  private getRepairsInProgressInLocalStorage(): RepairsInProgress {
    return JSON.parse(<string>localStorage.getItem(this.REPAIRS_IN_PROGRESS_KEY));
  }
  
  private setRepairsInProgressInLocalStorage(): void {
    localStorage.setItem(this.REPAIRS_IN_PROGRESS_KEY, JSON.stringify(this.repairsInProgress));
  }
  
  public getRepairsInProgress(): RepairsInProgress { return this.repairsInProgress; }
  
  public hasRepairBeenCalled(repairType: RepairTypeType): boolean {
    let repairsInProgressIndex: number = 0;
    if (repairType === 'Mécanique') {
      repairsInProgressIndex = 1;
    } else if (repairType === 'Service-Minute') {
      repairsInProgressIndex = 2;
    }
    return this.repairsInProgress[repairsInProgressIndex].repairState === 'called';
  }
  
  public hasRepairStarted(repairType: RepairTypeType): boolean {
    let repairsInProgressIndex: number = 0;
    if (repairType === 'Mécanique') {
      repairsInProgressIndex = 1;
    } else if (repairType === 'Service-Minute') {
      repairsInProgressIndex = 2;
    }
    return this.repairsInProgress[repairsInProgressIndex].repairState === 'started';
  }
  
  public haveAllRepairsStarted(): boolean {
    return this.hasRepairStarted('Carrosserie') && this.hasRepairStarted('Mécanique') && this.hasRepairStarted('Service-Minute');
  }
}
