import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RepairTypeType } from 'src/app/types/repairTypeType.type';
import { environment } from 'src/environments/environment';
import { SecurityService } from '../security/security.service';

@Injectable({
  providedIn: 'root'
})
export class RepairRESTService {
  private AUTH_HEADER: Object = {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} };
  
  constructor(private http: HttpClient, private securityService: SecurityService) { }
  
  public addClient(clientName: string): Observable<object> {
    return this.http.post(environment.BACK_URL + '/clients/add', {clientName},
    {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} });
  }
  
  public addCar(registration: string): Observable<object> {
    return this.http.post(environment.BACK_URL + '/cars/add', {registration},
    {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} });
  }
  
  public linkCarToClient(clientName: string, registration: string): Observable<object> {
    return this.http.post(environment.BACK_URL + '/clients/cars/add', {clientName, registration},
    {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} });
  }
  
  public addRepair(clientName: string, registration: string, repairType: RepairTypeType): Observable<object> {
    return this.http.post(environment.BACK_URL + '/repairs/add', {clientName, registration, repairType},
    {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} });
  }
  
  public getCarInfos(registration: string): Observable<object> {
    return this.http.get("http://api.apiplaqueimmatriculation.com/carte-grise?immatriculation=" + registration + "&token=TokenDemo&format=json");
  }
  
  public getRepairsList(): Observable<object> {
    return this.http.get(environment.BACK_URL + '/repairs/get',
    {headers: {Authorization: "Bearer " + this.securityService.getTokenInLocalStorage()} });
  }
}
