import { TestBed } from '@angular/core/testing';

import { RepairRESTService } from './repair-rest.service';

describe('RepairRESTService', () => {
  let service: RepairRESTService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RepairRESTService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
