import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  private TOKEN_KEY: string = 'TOKEN';
  private isTokenFromLocalStorageSubject: Subject<string> = new Subject<string>();
  
  constructor(private http: HttpClient) { }
  
  public sendRegisterRequest(email: string, password: string, ownerCode: string): Observable<any> {
    return this.http.post(environment.BACK_URL + '/register', {email, password, ownerCode});
  }
  
  public sendLoginRequest(email: string, password: string): Observable<any> {
    return this.http.post(environment.BACK_URL + '/login', {email, password});
  }
  
  // Connexion
  public setTokenInLocalStorage(token: string): void {
    localStorage.setItem(this.TOKEN_KEY, token);
    this.emitIsTokenFromLocalStorageSubject();
  }
  
  public getTokenInLocalStorage(): string { return localStorage.getItem(this.TOKEN_KEY) || ""; }
  
  // Déconnexion
  public removeTokenFromLocalStorage(): void {
    localStorage.removeItem(this.TOKEN_KEY);
    this.emitIsTokenFromLocalStorageSubject();
  }
  
  public isAuth(): boolean { return localStorage.getItem(this.TOKEN_KEY) !== null; }
  
  public getRole(token: string): string {
    const DECODED_TOKEN: any = jwt_decode(token);
    return DECODED_TOKEN.role;
  }
  
  private emitIsTokenFromLocalStorageSubject(): void {
    this.isTokenFromLocalStorageSubject.next();
  }
  
  public getIsTokenFromLocalStorageSubject(): Subject<string> {
    return this.isTokenFromLocalStorageSubject;
  }
}
