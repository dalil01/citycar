import { RepairTypeType } from "../types/repairTypeType.type";

export class Repair {
    public repairType: RepairTypeType;
    public clientName: string;
    public registration: string;
    
    constructor(repairType: RepairTypeType, clientName: string = "", registration: string = "") {
        this.repairType = repairType; this.clientName = clientName; this.registration = registration;
    }
}
